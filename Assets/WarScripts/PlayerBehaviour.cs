﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    public float Force = 1000f;

    private Rigidbody _rigid;

    // Start is called before the first frame update
    void Start()
    {

        _rigid = GetComponent<Rigidbody>();

        
    }

    // Update is called once per frame
    void FixedUpdate()
    {

       float xInput = Input.GetAxisRaw("Horizontal");
       float zINput = Input.GetAxisRaw("Vertical"); 

       _rigid.AddForce(new Vector3(xInput, 0, zINput) * Time.deltaTime * Force);
        
    }
}
