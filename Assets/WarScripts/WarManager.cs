﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarManager : MonoBehaviour
{

    public List<RobotData> Robots = new List<RobotData>();
    public List<LaneData> Lanes = new List<LaneData>();


    [System.Serializable]
    public struct RobotData {

        public string name;
        public GameObject Prefab;
        public GameObject Drop;
    }

    [System.Serializable]
    public struct LaneData {

        public float ZDepth;

        public Vector3 LeftSpawnPosition;
        public Vector3 RightSpawnPosition;
    }


    public static WarManager Instance;

    public float SpawnInterval = 3f;

    private float _lastSpawn = -99f;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance == null) {
            Instance = this;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > _lastSpawn + SpawnInterval)
        {
            _lastSpawn = Time.time;

            SpawnRobot(Side.Left);
            SpawnRobot(Side.Right);
        }
    }


    private void SpawnRobot(Side side) {

        GameObject robot = (GameObject)Instantiate(Robots[0].Prefab);

        int laneNumber = (int)Random.Range(0, 4);

        if (side == Side.Left) {
            robot.transform.position = Lanes[laneNumber].LeftSpawnPosition;
        }
        else
        {
            robot.transform.position = Lanes[laneNumber].RightSpawnPosition;
            robot.transform.rotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
        }



    }

    private enum Side {
        Left,
        Right,
    }
}
